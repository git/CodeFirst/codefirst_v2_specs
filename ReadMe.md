# codefirst v2 - spécifications
Voici la liste des caractéristiques que nous souhaitons mettre en place dans codefirst v2.

Le code de la v2 sera open-source prochainement. Si vous souhaitez participer dès maintenant, merci de me contacter.

## authentification
- keycloak
- (comme sur la v1)
- branché au LDAP de l'UCA

## git intégré
- à travers GitLab
- (à la place de gitea)

## gestion du pipeline de CI/CD
- à travers GitLab  
- (à la place de Drone)
- en utilisant ```.gitlab-ci.yml```

## analyseur de code
- SonarQube
- (comme sur la v1)

## gestion de projets
- gestion de tickets (via gitlab)
- gestion par projet (Kanban)

## hébergement de documentation
1. documentation privée par dépôt (GitLab pages)
2. documentation publique et partagée (pour les cours par exemple) via le déploiement de conteneur
3. ?? documentation accessible par authentification avec gestion des personnes autorisées ??

## déploiement des conteneurs et hébergement
- solution maison
- basée sur docker et kubernetes

## gestion des utilisateurs et des groupes
- utilisateurs et groupes directement remontés depuis Odin
- possibilité d'ajouter des utilisateurs externes (vacataires non encore répertoriés, partenaires externes...)

## portail développeur
### utilisateur
#### demandes et tickets
- Possibilité d'ajouter des tickets sur code first en général, pour :
  - déclarer un bug,
  - faire une demande de nouvelle *feature*
  - faire une demande de nouvel article dans la doc de code first
- possibilité de demander plus d'espace
#### gestion des conteneurs
- possibilité de voir ses conteneurs
- ? possibilité de relancer ses conteneurs ? (il peut déjà le faire en relançant son pipeline)
- possibilité de tuer ses conteneurs
#### possibilité de gérer ses documentations publiques
- lister ses documentations
- supprimer une documentation
#### déploiement
- paramétrage kub (récupérer sa config pour son namespace (perso ou groupe))
- créer/supprimer un namespace pour un groupe gitlab
### admin
#### gestion des demandes et tickets
- ? lister les tickets ? (déjà visible depuis gitlab)
- modifier l'espace alloué à un utilisateur
#### gestion des conteneurs
- possibilité de tuer les conteneurs de n'importe qui
- possibilité de voir tous les conteneurs
- possibilité de relancer n'importe quel conteneur
#### gestion des documentations publiques
- lister les documentations par utilisateur
- supprimer une documentation

## installation 
- code open-source et accessible sous GitHub
- doc pour installer et relancer code first

## doc utilisateur
- documentation pour tous
- (comme sur la v1)
- hébergerment de cours (comme sur la v1)

## migration de code first v1 vers code first v2
- les dépôts et comptes seront automatiquement migrés de la v1 vers la v2

## ? matrix ?
- devons-nous remettre un tchat officielle code first / dpt ?
- celui-ci pourrait créer les "rooms" automatiquement par dépôt en mettant à jour la liste des membres de la "room" en fonction des membres ayant accès au dépôt
- pour le moment, sauf demande, pas à l'ODJ

## ? pastebin ?
- pour permettre l'échange rapide de code
- pas à l'ordre du jour pour le moment
